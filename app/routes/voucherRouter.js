//Khai báo thư viện express
const express = require('express');
const { getAllVouchers, getVoucherById, createVoucher, updateVoucherById, deleteVoucherById } = require('../controllers/voucherController');

//Tạo router
const voucherRouter = express.Router();

//Router get all voucher:
voucherRouter.get('/vouchers', getAllVouchers)

//Router get voucher by id:
voucherRouter.get('/vouchers/:voucherId', getVoucherById)

//Router post voucher(create new voucher)
voucherRouter.post('/vouchers', createVoucher)

//Router put voucher (update voucher by id)
voucherRouter.put('/vouchers/:voucherId', updateVoucherById)

//Router delete voucher (delete voucher by Id):
voucherRouter.delete('/vouchers/:voucherId', deleteVoucherById)

module.exports = {voucherRouter}