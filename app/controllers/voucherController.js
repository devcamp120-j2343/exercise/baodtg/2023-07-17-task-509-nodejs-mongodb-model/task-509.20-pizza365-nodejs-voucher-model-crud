//import voucher model
const { default: mongoose, mongo } = require('mongoose');
const voucherModel = require('../models/voucherModel');
const drinkModel = require('../models/drinkModel');

//Get all vouchers:
const getAllVouchers = (req, res) => {
    voucherModel.find()
        .then((data) => {
            if (data && data.lenght > 0) {
                return res.status(200).json({
                    status: `Get all vouchers successfully`,
                    data
                })
            } else {
                return res.status(404).json({
                    status: `Not found any vouchers`,
                    data
                })
            }
        })
}

//Get voucher by Id:
const getVoucherById = (req, res) => {
    //B1: thu thập dữ liệu:
    const voucherId = req.params.voucherId;
    //B2: kiểm tra dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Voucher id invalid`
        })
    }
    //B3: thực thi model:
    voucherModel.findById(voucherId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: `Get voucher by Id successfully`,
                    data
                })
            } else {
                return res.status(404).json({
                    status: `Not found any vouchers`,
                    data
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: `Internal Server Error`,
                message: error.message
            })
        })
}

//Create new voucher:
const createVoucher = (req, res) => {
    //B1: thu thập dữ liệu
    let { maVoucher, phanTramGiamGia, ghiChu } = req.body
    //B2: kiểm tra dữ liệu:
    if (!maVoucher) {
        return res.status(400).json({
            message: `maVoucher is required`
        })
    }
    if (!Number.isInteger(phanTramGiamGia) || phanTramGiamGia > 90) {
        return res.status(400).json({
            message: `phanTramGiamGia is invalid`
        })
    }
    //B3: thực thi model:
    let newVoucher = new voucherModel({
        _id: new mongoose.Types.ObjectId(),
        maVoucher: maVoucher,
        phanTramGiamGia: phanTramGiamGia,
        ghiChu: ghiChu
    })
    voucherModel.create(newVoucher)
        .then((data) => {
            return res.status(201).json({
                status: `Create new voucher successfully`,
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: `Internal Server Error`,
                message: error.message
            })
        })
}

//Update voucher by Id:
const updateVoucherById = (req, res) => {
    //B1: thu thập dữ liệu
    const voucherId = req.params.voucherId;
    let { maVoucher, phanTramGiamGia, ghiChu } = req.body
    //B2: kiểm tra dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `voucher Id invalid`
        })
    }
    if (!maVoucher) {
        return res.status(400).json({
            message: `maVoucher is required`
        })
    }
    if (!Number.isInteger(phanTramGiamGia) || phanTramGiamGia > 90) {
        return res.status(400).json({
            message: `phanTramGiamGia is invalid`
        })
    }

    let updateVoucherData = {
        maVoucher,
        phanTramGiamGia,
        ghiChu
    }
    voucherModel.findByIdAndUpdate(voucherId, updateVoucherData)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: `Update voucher by Id successfully`,
                    data
                })
            } else {
                return res.status(404).json({
                    status: `Not found any vouchers`,
                    data
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: `Internal Server Error`,
                message: error.message
            })
        })

}

//Delete voucher by Id:
const deleteVoucherById = (req, res) => {
    //B1: thu thập dữ liệu
    const voucherId = req.params.voucherId;
    //B2: kiểm tra dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `voucher id invalid`
        })
    }
    //B3: thực thi model:
    voucherModel.findByIdAndDelete(voucherId)
    .then((data) => {
        if(data) {
            return res.status(204).json({
                status: `delete voucher by Id successfully`,
                data
            })
        }else {
            return res.status(404).json({
                status: `not found any vouchers`,
                data
            })
        }
    })
    .catch((error) => {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    })

}

module.exports = { getAllVouchers, getVoucherById, createVoucher, updateVoucherById, deleteVoucherById }