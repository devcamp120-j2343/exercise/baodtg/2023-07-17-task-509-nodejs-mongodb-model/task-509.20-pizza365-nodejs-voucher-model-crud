//Khai báo thư viện express:
const express = require('express');

//Khai báo model mongoose:
const drinkModel = require('./app/models/drinkModel')
const voucherModel = require('./app/models/voucherModel')
const orderModel = require('./app/models/orderModel')
const userModel = require('./app/models/userModel')


//Khai báo router:
const { drinkRouter } = require('./app/routes/drinkRouter');
const { voucherRouter } = require('./app/routes/voucherRouter');
const { userRouter } = require('./app/routes/userRouter');
const { orderRouter } = require('./app/routes/orderRouter');

//Khai báo thư viện mongoose
var mongoose = require('mongoose');

//Khai báo app:
const app = express();
app.use(express.json())

//Khai báo port:
const port = 8000;

//kết nối mongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch(error => handleError(error))

app.use('/', drinkRouter)
app.use('/', voucherRouter)
app.use('/', userRouter)
app.use('/', orderRouter)

//Khởi động app:
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})